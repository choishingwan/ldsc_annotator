// This file is part of PRSice-2, copyright (C) 2016-2019
// Shing Wan Choi, Paul F. O’Reilly
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef GENOTYPE_H
#define GENOTYPE_H

#include "IITree.h"
#include "commander.hpp"
#include "misc.hpp"
#include "plink_common.hpp"
#include "reporter.hpp"
#include "snp.hpp"
#include "storage.hpp"
#include <Eigen/Dense>
#include <algorithm>
#include <atomic>
#include <cctype>
#include <cstdio>
#include <cstring>
#include <deque>
#include <fstream>
#include <functional>
#include <memory>
#include <mutex>
#include <random>
#include <set>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#ifdef __APPLE__
#include <sys/sysctl.h> // sysctl()
#endif


#define MULTIPLEX_LD 1920
#define MULTIPLEX_2LD (MULTIPLEX_LD * 2)
class Genotype
{
public:
    /*!
     * \brief default constructor of Genotype
     */
    Genotype() {}
    /*!
     * \brief Genotype constructor
     * \param thread = number of thread
     * \param ignore_fid = if FID should be ignored
     * \param keep_nonfounder = if we should keep non-founders
     * \param keep_ambig = if we should keep ambiguous SNPs
     * \param is_ref = if this is a reference genome
     */
    Genotype(const uint32_t thread, const bool ignore_fid,
             const bool keep_nonfounder, const bool keep_ambig,
             const bool is_ref = false)
        : m_thread(thread)
        , m_ignore_fid(ignore_fid)
        , m_is_ref(is_ref)
        , m_keep_nonfounder(keep_nonfounder)
        , m_keep_ambig(keep_ambig)
    {
    }
    virtual ~Genotype();

    // after load samples, samples that we don't want to include
    // will all have their FID, IID and phenotype masked by empty string
    /*!
     * \brief Function to load samples into the genotype class object. Will call
     *        gen_sample_vector. Will only store the sample info if this is the
     *        target.
     * \param keep_file is the file containing samples to be included
     * \param remove_file is the file containing samples to be excluded
     * \param verbose boolean indicate whether we should output the count
     * \param reporter is the logger
     */
    void load_samples(bool verbose = true);
    // We need the exclusion_region parameter because when we read in the base
    // we do allow users to provide a base without the CHR and LOC, which forbid
    // us to do the regional filtering. However, as exclusion and extractions
    // are based on the SNP ID, we can be confident that they were already done
    // in read_base
    void load_snps(const std::string& out,
                   const std::vector<IITree<size_t, size_t>>& exclusion_regions,
                   bool verbose);
    std::tuple<size_t, size_t> get_max_id_length() const
    {
        return {m_max_fid_length, m_max_iid_length};
    }

    /*!
     * \brief Return the number of SNPs, use for unit test
     * \return reuturn the number of SNPs included
     */
    size_t num_snps() const { return m_existed_snps.size(); }
    /*!
     * \brief Function to re-propagate the m_existed_snps_index after reading
     * the reference panel
     */
    void update_snp_index()
    {
        m_existed_snps_index.clear();
        for (size_t i_snp = 0; i_snp < m_existed_snps.size(); ++i_snp)
        { m_existed_snps_index[m_existed_snps[i_snp]->rs()] = i_snp; }
    }
    /*!
     * \brief Return the number of sample we wish to perform PRS on
     * \return the number of sample
     */
    size_t num_sample() const { return m_sample_id.size(); }

    /*!
     * \brief Function to prepare clumping. Should sort all the SNPs by their
     * chromosome number and then by their p-value
     * \return true if there are SNPs to sort
     */
    bool sort_by_p()
    {
        if (m_existed_snps.size() == 0) return false;
        m_sort_by_p_index = SNP::sort_by_p_chr(m_existed_snps);
        return true;
    }

    static bool chr_prefix(std::string_view str)
    {
        if (str.length() <= 3) { return false; }
        return ((str[0] & 0xdf) == 'C' && (str[1] & 0xdf) == 'H'
                && (str[2] & 0xdf) == 'R');
    }
    static int32_t get_chrom_code(std::string_view str)
    {
        if (chr_prefix(str)) { str.remove_prefix(3); }
        if (str.length() == 0) return -1;
        if ((str[0] & 0xdf) == 'X')
        {
            if (str.length() == 2 && (str[1] & 0xdf) == 'Y')
                return CHROM_XY;
            else if (str.length() == 1)
                return CHROM_X;
        }
        else if (str.length() == 1 && (str[0] & 0xdf) == 'Y')
        {
            return CHROM_Y;
        }
        else if ((str[0] & 0xdf) == 'M')
        {
            if (str.length() == 1
                || (str.length() == 2 && (str[1] & 0xdf) == 'T'))
                return CHROM_MT;
        }
        else if (str[0] == '0')
        {
            if (str.length() == 2)
            {
                switch (str[1] & 0xdf)
                {
                case 'X': return CHROM_X;
                case 'Y': return CHROM_Y;
                case 'M': return CHROM_MT;
                }
            }
        }
        try
        {
            return misc::Convertor::convert<int32_t>(std::string(str));
        }
        catch (const std::runtime_error&)
        {
            return -1;
        }
    }


    std::string
    print_duplicated_snps(const std::unordered_set<std::string>& snp_name,
                          const std::string& out_prefix);


    bool parse_chr(const std::vector<std::string_view>& token,
                   const BaseFile& base_file, std::vector<size_t>& filter_count,
                   size_t& chr)
    {
        if (filter_count.size() != +FILTER_COUNT::MAX)
        { filter_count.resize(+FILTER_COUNT::MAX, 0); }
        chr = ~size_t(0);
        if (!base_file.has_column[+BASE_INDEX::CHR]) return true;
        int32_t chr_code =
            get_chrom_code(token[base_file.column_index[+BASE_INDEX::CHR]]);
        if (chr_code < 0)
        {
            ++filter_count[+FILTER_COUNT::CHR];
            return false;
        }
        if (static_cast<size_t>(chr_code) > m_autosome_ct
            || is_set(m_haploid_mask.data(), static_cast<uint32_t>(chr_code)))
        {
            ++filter_count[+FILTER_COUNT::HAPLOID];
            return false;
        }
        chr = static_cast<size_t>(chr_code);
        return true;
    }


    bool parse_loc(const std::vector<std::string_view>& token,
                   const BaseFile& base_file, size_t& loc)
    {
        loc = ~size_t(0);
        if (!base_file.has_column[+BASE_INDEX::BP]) return true;
        try
        {
            loc = misc::Convertor::convert<size_t>(
                std::string(token[base_file.column_index[+BASE_INDEX::BP]])
                    .c_str());
        }
        catch (...)
        {
            return false;
        }
        return true;
    }
    void init_sample_vectors()
    {
        if (m_unfiltered_sample_ct == 0)
        {
            throw std::runtime_error("Error: No sample found in genotype file");
        }
        const uintptr_t unfiltered_sample_ctl =
            BITCT_TO_WORDCT(m_unfiltered_sample_ct);
        m_sample_for_ld.resize(unfiltered_sample_ctl, 0);
        m_calculate_prs.resize(unfiltered_sample_ctl, 0);
        m_num_male = 0;
        m_num_female = 0;
        m_num_ambig_sex = 0;
        m_num_non_founder = 0;
        m_sample_ct = 0;
        m_founder_ct = 0;
        m_vector_initialized = true;
    }
    void post_sample_read_init()
    {
        if (m_unfiltered_sample_ct == 0)
        {
            throw std::runtime_error("Error: No sample found in genotype file");
        }
        const uintptr_t unfiltered_sample_ctl =
            BITCT_TO_WORDCT(m_unfiltered_sample_ct);
        const uintptr_t unfiltered_sample_ctv2 = 2 * unfiltered_sample_ctl;
        m_tmp_genotype.resize(unfiltered_sample_ctv2, 0);
        m_prs_info.resize(m_sample_ct, PRS());
        m_sample_include2.resize(unfiltered_sample_ctv2, 0);
        m_founder_include2.resize(unfiltered_sample_ctv2, 0);
        // fill it with the required mask (copy from PLINK2)
        init_quaterarr_from_bitarr(m_calculate_prs.data(),
                                   m_unfiltered_sample_ct,
                                   m_sample_include2.data());
        init_quaterarr_from_bitarr(m_sample_for_ld.data(),
                                   m_unfiltered_sample_ct,
                                   m_founder_include2.data());
        m_exclude_from_std.resize(unfiltered_sample_ctl, 0);
    }

    std::vector<std::pair<size_t, size_t>> get_chrom_boundary();


    /*!
     * \brief Before each run of PRSice, we need to reset the in regression
     * flag to false and propagate it later on to indicate if the sample is
     * used in the regression model
     */

    void reset_in_regression_flag()
    {
        std::fill(m_in_regression.begin(), m_in_regression.end(), 0);
    }
    void reset_std_flag()
    {
        std::fill(m_exclude_from_std.begin(), m_exclude_from_std.end(), 0);
    }
    void exclude_from_std(const size_t idx)
    {
        SET_BIT(idx, m_exclude_from_std.data());
    }


    /*!
     * \brief This function will return the sample ID
     * \param i is the index of the sample
     * \return the sample ID
     */
    std::string sample_id(const size_t i, const std::string& delim) const
    {
        if (i > m_sample_id.size())
            throw std::out_of_range("Sample name vector out of range");
        return m_sample_id[i].FID + delim + m_sample_id[i].IID;
    }

    bool in_regression(size_t i) const
    {
        return m_sample_id.at(i).in_regression;
    }

    // as we don't check i is within range, the following 3 functions
    // have a potential of array out of bound
    /*!
     * \brief Check if the i th sample has been included in regression
     * \param i is the index of the sample
     * \return true if it is included
     */
    bool sample_in_regression(size_t i) const
    {
        return IS_SET(m_in_regression.data(), i);
    }
    /*!
     * \brief Inform PRSice that the i th sample has been included in the
     * regression
     *
     * \param i is the sample index
     */
    void set_in_regression(size_t i) { SET_BIT(i, m_in_regression.data()); }
    /*!
     * \brief Return the phenotype stored in the fam file of the i th sample
     * \param i is the index of the  sample
     * \return the phenotype of the sample
     */
    std::string pheno(size_t i) const { return m_sample_id[i].pheno; }
    /*!
     * \brief This function return if the i th sample has NA as phenotype
     * \param i is the sample ID
     * \return true if the phenotype is NA
     */
    bool pheno_is_na(size_t i) const
    {
        auto pheno = m_sample_id[i].pheno;
        misc::to_lower(pheno);
        return pheno == "na" || pheno == "nan";
    }
    void update_valid_sample(size_t idx, bool valid_phenotype)
    {
        m_sample_id.at(idx).valid_phenotype = valid_phenotype;
    }
    bool sample_selected_for_prs(size_t idx) const
    {
        return m_sample_id.at(idx).in_regression;
    }
    bool sample_valid_for_regress(size_t idx) const
    {
        return m_sample_id.at(idx).valid_phenotype;
    }
    /*!
     * \brief Return the fid of the i th sample
     * \param i is the sample index
     * \return FID of the i th sample
     */
    std::string fid(size_t i) const { return m_sample_id.at(i).FID; }
    /*!
     * \brief Return the iid fo the i th sample
     * \param i is the sample index
     * \return IID of the i th sample
     */
    std::string iid(size_t i) const { return m_sample_id.at(i).IID; }
    /*!
     * \brief return the largest chromosome allowed
     * \return  the largest chromosome
     */
    uint32_t max_chr() const { return m_max_code; }
    /*!
     * \brief Indicate we will be using a reference file. Use for bgen
     * intermediate output generation
     */
    void expect_reference() { m_expect_reference = true; }

    void build_membership_matrix(const size_t num_sets,
                                 const std::vector<std::string>& region_name,
                                 std::unique_ptr<std::ostream> out);


    static bool within_region(const std::vector<IITree<size_t, size_t>>& cr,
                              const size_t chr, const size_t loc)
    {
        std::vector<size_t> output;
        if (chr >= cr.size()) return false;
        size_t start = (loc == 0) ? 1 : loc;
        cr[chr].overlap(start, loc + 1, output);
        return !output.empty();
    }


    static void
    construct_flag(const std::vector<IITree<size_t, size_t>>& gene_sets,
                   const size_t required_size,
                   const bool genome_wide_background, std::unique_ptr<SNP>& snp)
    {
        auto& flag = snp->get_flag();
        if (flag.size() != required_size) { flag.resize(required_size); }
        std::fill(flag.begin(), flag.end(), 0);
        SET_BIT(0, flag.data());
        if (genome_wide_background) { SET_BIT(1, flag.data()); }
        if (gene_sets.empty()) return;
        if (snp->chr() >= gene_sets.size()) return;
        std::vector<size_t> out;
        if (!gene_sets[snp->chr()].has_overlap(snp->loc(), out)) return;
        for (auto&& idx : out) { SET_BIT(idx, flag.data()); }
    }

    void add_flags(const std::vector<IITree<size_t, size_t>>& cr,
                   const size_t num_sets, const bool genome_wide_background);

    // Refactoring
    Genotype& keep_nonfounder(bool keep)
    {
        m_keep_nonfounder = keep;
        return *this;
    }
    Genotype& keep_ambig(bool keep)
    {
        m_keep_ambig = keep;
        return *this;
    }
    Genotype& reference()
    {
        m_is_ref = true;
        return *this;
    }
    Genotype& intermediate(bool use)
    {
        m_intermediate = use;
        return *this;
    }
    Genotype& set_prs_instruction(const CalculatePRS& prs)
    {
        m_has_prs_instruction = true;
        m_prs_calculation = prs;
        return *this;
    }
    void snp_extraction(const std::string& extract_snps,
                        const std::string& exclude_snps);


    class dummy_reporter
    {
        bool m_completed = false;
        bool m_verbose = true;
        size_t m_total_snp = 0;
        size_t m_processed = 0;
        double m_prev_progress = 0;

    public:
        dummy_reporter(size_t total, bool verbose = true)
            : m_verbose(verbose), m_total_snp(total)
        {
        }
        void emplace(size_t&& item)
        {
            m_processed += item;
            auto progress = (static_cast<double>(m_processed)
                             / static_cast<double>(m_total_snp))
                            * 100;
            if (m_verbose && progress - m_prev_progress > 0.01)
            {
                fprintf(stderr, "\rClumping Progress: %03.2f%%", progress);
                m_prev_progress = progress;
            }
        }
        void completed() { m_completed = true; }
    };
    Genotype& set_weight(const MODEL& genetic_model)
    {
        switch (genetic_model)
        {
        case MODEL::HETEROZYGOUS:
            m_homcom_weight = 0;
            m_het_weight = 1;
            m_homrar_weight = 0;
            break;
        case MODEL::DOMINANT:
            m_homcom_weight = 0;
            m_het_weight = 1;
            m_homrar_weight = 1;
            break;
        case MODEL::RECESSIVE:
            m_homcom_weight = 0;
            m_het_weight = 0;
            m_homrar_weight = 1;
            break;
        default:
            m_homcom_weight = 0;
            m_het_weight = 1;
            m_homrar_weight = 2;
            break;
        }
        return *this;
    }

    void set_thresholds(const QCFiltering& qc)
    {
        m_hard_threshold = qc.hard_threshold;
        m_dose_threshold = qc.dose_threshold;
    }
    void load_genotype_to_memory();
    bool genotyped_stored() const { return m_genotype_stored; }
    const std::unordered_map<std::string, size_t>& included_snps_idx() const
    {
        return m_existed_snps_index;
    }
    const std::vector<std::unique_ptr<SNP>>& included_snps() const
    {
        return m_existed_snps;
    }
    void parse_chr_id_formula(const std::string& chr_id_formula);


protected:
    // friend with all child class so that they can also access the
    // protected elements
    friend class BinaryPlink;
    friend class BinaryGen;
    std::vector<std::unique_ptr<SNP>> m_existed_snps;
    std::unordered_map<std::string, size_t> m_existed_snps_index;
    std::unordered_set<std::string> m_sample_selection_list;
    std::unordered_set<std::string> m_snp_selection_list;
    std::vector<std::set<double>> m_set_thresholds;
    std::vector<Sample_ID> m_sample_id;
    std::vector<PRS> m_prs_info;
    std::vector<std::string> m_genotype_file_names;
    std::vector<char> m_chr_id_symbol;
    std::vector<uintptr_t> m_tmp_genotype;
    // std::vector<uintptr_t> m_chrom_mask;
    std::vector<uintptr_t> m_sample_for_ld;
    std::vector<uintptr_t> m_founder_include2;
    std::vector<uintptr_t> m_calculate_prs;
    std::vector<uintptr_t> m_sample_include2;
    std::vector<uintptr_t> m_exclude_from_std;
    std::vector<uintptr_t> m_in_regression;
    std::vector<uintptr_t> m_haploid_mask;
    std::vector<size_t> m_sort_by_p_index;
    std::vector<int> m_chr_id_column;
    // std::vector<uintptr_t> m_sex_male;
    std::vector<int32_t> m_xymt_codes;
    std::ofstream m_mismatch_snp_record;
    // std::vector<int32_t> m_chrom_start;
    // sample file name. Fam for plink
    std::string m_sample_file;
    std::string m_delim;
    std::string m_keep_file;
    std::string m_remove_file;
    double m_mean_score = 0.0;
    double m_score_sd = 0.0;
    double m_hard_threshold = 0.0;
    double m_dose_threshold = 0.0;
    double m_homcom_weight = 0;
    double m_het_weight = 1;
    double m_homrar_weight = 2;
    size_t m_num_thresholds = 0;
    size_t m_thread = 1; // number of final samples
    size_t m_max_window_size = 0;
    size_t m_num_ambig = 0;
    size_t m_num_maf_filter = 0;
    size_t m_num_geno_filter = 0;
    size_t m_num_info_filter = 0;
    size_t m_num_miss_filter = 0;
    size_t m_num_xrange = 0;
    size_t m_base_missed = 0;
    size_t m_num_male = 0;
    size_t m_num_female = 0;
    size_t m_num_ambig_sex = 0;
    size_t m_num_non_founder = 0;
    size_t m_max_fid_length = 3;
    size_t m_max_iid_length = 3;
    uintptr_t m_unfiltered_sample_ct = 0; // number of unfiltered samples
    uintptr_t m_unfiltered_marker_ct = 0;
    uintptr_t m_sample_ct = 0;
    uintptr_t m_founder_ct = 0;
    uintptr_t m_marker_ct = 0;
    uint32_t m_max_category = 0;
    uint32_t m_autosome_ct = 0;
    uint32_t m_max_code = 0;
    std::random_device::result_type m_seed = 0;
    uint32_t m_num_ref_target_mismatch = 0;
    bool m_genotype_stored = false;
    bool m_use_proxy = false;
    bool m_has_prs_instruction = false;
    bool m_ignore_fid = false;
    bool m_intermediate = false;
    bool m_is_ref = false;
    bool m_keep_nonfounder = false;
    bool m_keep_ambig = false;
    bool m_remove_sample = true;
    bool m_exclude_snp = true;
    bool m_hard_coded = false;
    bool m_expect_reference = false;
    bool m_memory_initialized = false;
    bool m_very_small_thresholds = false;
    bool m_vector_initialized = false;
    bool m_has_chr_id_formula = false;
    Reporter* m_reporter = nullptr;
    CalculatePRS m_prs_calculation;

    std::string initialize(const GenoFile& geno, const Phenotype& pheno,
                           const std::string& delim, const std::string& type,
                           Reporter* reporter)
    {
        m_sample_file = "";
        m_ignore_fid = pheno.ignore_fid;
        m_keep_file = geno.keep;
        m_remove_file = geno.remove;
        m_delim = delim;
        m_reporter = reporter;
        init_chr(geno.num_autosome);
        const bool use_list = !(geno.file_list.empty());
        const std::string file_name =
            use_list ? geno.file_list : geno.file_name;
        std::vector<std::string> token = misc::split(file_name, ",");
        if (token.empty())
        {
            throw std::runtime_error("Error: Invalid user input: " + file_name);
        }
        const bool external_sample = (token.size() == 2);
        if (token.size() > 2)
        {
            throw std::runtime_error("Error: Undefine user input: "
                                     + file_name);
        }
        // check for empty input
        for (auto&& t : token)
        {
            if (t.empty())
            {
                throw std::runtime_error("Error: Invalid user input: "
                                         + file_name);
            }
        }
        std::string message = "Initializing Genotype ";
        if (use_list)
        {
            auto input = misc::load_stream(token[0]);
            // we move input to function, can no longer use it
            m_genotype_file_names = load_genotype_prefix(std::move(input));
            message.append("info from file: " + token[0] + " (" + type + ")\n");
        }
        else
        {
            m_genotype_file_names = set_genotype_files(token[0]);
            message.append("file: " + token[0] + " (" + type + ")\n");
        }
        if (external_sample)
        {
            m_sample_file = token[1];
            message.append("With external fam file: " + m_sample_file + "\n");
        }
        return message;
    }
    std::string chr_id_from_genotype(const std::unique_ptr<SNP>& snp) const;
    std::string
    get_chr_id_from_base(const BaseFile& base_file,
                         const std::vector<std::string_view>& token);
    bool has_parent(const std::unordered_set<std::string>& founder_info,
                    const std::vector<std::string>& token,
                    const std::string& fid, const size_t idx);
    void gen_sample(const size_t fid_idx, const size_t iid_idx,
                    const size_t sex_idx, const size_t dad_idx,
                    const size_t mum_idx, const size_t cur_idx,
                    const std::unordered_set<std::string>& founder_info,
                    const std::string& pheno, std::vector<std::string>& token,
                    std::vector<Sample_ID>& sample_storage,
                    std::unordered_set<std::string>& sample_in_file,
                    std::vector<std::string>& duplicated_sample_id);

    void print_mismatch(const std::string& out, const std::string& type,
                        const std::unique_ptr<SNP>& target,
                        const std::unique_ptr<SNP>& new_snp);

    bool snp_dup_selection_check(const std::string& chr_id, std::string& id,
                                 std::unordered_set<std::string>& processed_idx,
                                 std::unordered_set<std::string>& dup_rs,
                                 std::vector<size_t>& filter_count)
    {
        // resize the counting vector if it isn't the correct size
        if (filter_count.size() != +FILTER_COUNT::MAX)
        { filter_count.resize(+FILTER_COUNT::MAX, 0); }
        // if rs id isn't provided, use chr_id for all
        if (id.empty()) id = chr_id;
        // if chr_id or the rs_id is already found, then
        // add it to the duplicated list. Duplicated SNPs might be
        // more of a problem for chr_id as it is more likely to have
        // two SNPs with same coordinates than having two different
        // SNPs to have same coordinates
        if (processed_idx.find(id) != processed_idx.end()
            || (!chr_id.empty()
                && processed_idx.find(chr_id) != processed_idx.end()))
        {
            ++filter_count[+FILTER_COUNT::DUP_SNP];
            dup_rs.insert(id);
            return false;
        }
        // check for extraction / exclusion using rs_id first
        auto&& selection = m_snp_selection_list.find(id);
        if (m_exclude_snp && selection != m_snp_selection_list.end())
        {
            // we want to exclude this snp
            ++filter_count[+FILTER_COUNT::SELECT];
            return false;
        }
        else if (!m_exclude_snp && selection == m_snp_selection_list.end())
        {
            // want to include, but not found
            if (chr_id.empty())
            {
                ++filter_count[+FILTER_COUNT::SELECT];
                return false;
            }
            else
            {
                selection = m_snp_selection_list.find(chr_id);
                if ((!m_exclude_snp && selection == m_snp_selection_list.end())
                    || (m_exclude_snp
                        && selection != m_snp_selection_list.end()))
                {
                    ++filter_count[+FILTER_COUNT::SELECT];
                    return false;
                }
                else
                {
                    processed_idx.insert(chr_id);
                }
            }
        }
        // without selection file
        processed_idx.insert(id);
        return true;
    }

    std::string get_chr_id(const std::unique_ptr<SNP>& snp) const
    {
        // check if we have all the column we need
        std::string chr_id = "";
        for (auto col : m_chr_id_column)
        {
            if (col < 0)
            {
                auto idx = (-1 * col - 1);
                chr_id += m_chr_id_symbol[idx];
            }
            else
            {
                std::string str;
                switch (col)
                {
                case +BASE_INDEX::EFFECT:
                    str = snp->ref();
                    misc::to_upper(str);
                    chr_id += str;
                    break;
                case +BASE_INDEX::NONEFFECT:
                    str = snp->alt();
                    misc::to_upper(str);
                    chr_id += str;
                    break;
                case +BASE_INDEX::CHR:
                    str = std::to_string(snp->chr());
                    chr_id += str;
                    break;
                case +BASE_INDEX::BP:
                    str = std::to_string(snp->loc());
                    chr_id += str;
                    break;
                }
            }
        }
        return chr_id;
    }
    bool parse_rs_id(const std::vector<std::string_view>& token,
                     const BaseFile& base_file,
                     std::unordered_set<std::string>& processed_idx,
                     std::unordered_set<std::string>& dup_rs,
                     std::vector<size_t>& filter_count, std::string& rs_id,
                     std::string& chr_id)
    {
        // when chr_id is provided, we should use both the rs and chr id and
        // count it as extracted / excluded / successs whenever one of them
        // is matched, this allow flexibility esp w.r.t. situation when
        // there's duplicated SNPs
        chr_id = "";
        rs_id = "";
        if (!base_file.has_column[+BASE_INDEX::RS] && !m_has_chr_id_formula)
        { throw std::runtime_error("Error: RS ID column not provided!"); }
        if (base_file.has_column[+BASE_INDEX::RS])
        { rs_id = token[base_file.column_index[+BASE_INDEX::RS]]; }
        if (m_has_chr_id_formula)
        { chr_id = get_chr_id_from_base(base_file, token); }

        return (snp_dup_selection_check(chr_id, rs_id, processed_idx, dup_rs,
                                        filter_count));
    }

    void parse_allele(const std::vector<std::string_view>& token,
                      const BaseFile& base_file, size_t index,
                      std::string& allele)
    {
        allele = (base_file.has_column[index])
                     ? token[base_file.column_index[index]]
                     : "";
        misc::to_upper(allele);
    }

    bool parse_pvalue(const std::string_view& p_value_str,
                      const double max_threshold,
                      std::vector<size_t>& filter_count, double& pvalue)
    {
        if (filter_count.size() != +FILTER_COUNT::MAX)
        { filter_count.resize(+FILTER_COUNT::MAX, 0); }
        try
        {
            pvalue = misc::Convertor::convert<double>(std::string(p_value_str));
        }
        catch (...)
        {
            ++filter_count[+FILTER_COUNT::NOT_CONVERT];
            return false;
        }
        if (pvalue < 0.0 || pvalue > 1.0)
        {
            throw std::runtime_error("Error: Invalid p-value: "
                                     + std::string(p_value_str));
        }
        if (pvalue > max_threshold)
        {
            ++filter_count[+FILTER_COUNT::P_EXCLUDED];
            return false;
        }
        return true;
    }
    bool parse_stat(const std::string_view& stat_str, const bool odd_ratio,
                    std::vector<size_t>& filter_count, double& stat)
    {
        if (filter_count.size() != +FILTER_COUNT::MAX)
        { filter_count.resize(+FILTER_COUNT::MAX, 0); }
        try
        {
            stat = misc::Convertor::convert<double>(std::string(stat_str));
            if (odd_ratio && misc::logically_equal(stat, 0.0))
            {
                ++filter_count[+FILTER_COUNT::NOT_CONVERT];
                return false;
            }
            else if (odd_ratio && stat < 0.0)
            {
                ++filter_count[+FILTER_COUNT::NEGATIVE];
                return false;
            }
            else if (odd_ratio)
                stat = log(stat);
            return true;
        }
        catch (...)
        {
            ++filter_count[+FILTER_COUNT::NOT_CONVERT];
            return false;
        }
    }
    /*!
     * \brief Calculate the threshold bin based on the p-value and bound
     * info \param pvalue the input p-value \param bound_start is the start
     * of p-value threshold \param bound_inter is the step size of p-value
     * threshold \param bound_end is the end of p-value threshold \param
     * pthres return the name of p-value threshold this SNP belongs to
     * \param no_full indicate if we want the p=1 threshold
     * \return the category where this SNP belongs to
     */


    /*!
     * \brief Replace # in the name of the genotype file and generate list
     * of file for subsequent analysis \param prefix contains the name of
     * the genotype file \return a vector of string containing names of the
     * genotype files
     */
    std::vector<std::string> set_genotype_files(const std::string& prefix);
    /*!
     * \brief Read in the genotype list file and add the genotype file names
     * to the vector \param file_name is the name of the list file \return a
     * vector of string containing names of all genotype files
     */
    std::vector<std::string>
    load_genotype_prefix(std::unique_ptr<std::istream> in);
    /*!
     * \brief Initialize vector related to chromosome information
     * e.g.haplotype. Currently not really useful except for setting the
     * max_code which is later used to transform chromosome strings to
     * chromosome code \param num_auto is the number of autosome, we fix it
     * to 22 for human \param no_x indicate if chrX is missing for this
     * organism \param no_y indicate if chrY is missing for this organism
     * \param no_xy indicate if chrXY is missing for this organism
     * \param no_mt indicate if chrMT is missing for this organism
     */
    void init_chr(int num_auto = 22, bool no_x = false, bool no_y = false,
                  bool no_xy = false, bool no_mt = false);


    /*!
     * \brief Function to read in the sample. Any subclass must implement
     * this function. They \b must initialize the \b m_sample_info \b
     * m_founder_info \b m_founder_ct \b m_sample_ct \b m_prs_info \b
     * m_in_regression and \b m_tmp_genotype (optional) \return vector
     * containing the sample information
     */
    virtual std::vector<Sample_ID> gen_sample_vector()
    {
        return std::vector<Sample_ID>(0);
    }

    virtual void gen_snp_vector(
        const std::vector<IITree<size_t, size_t>>& /*exclusion_regions*/,
        const std::string& /*out_prefix*/)
    {
    }


    // for loading the sample inclusion / exclusion set
    /*!
     * \brief Function to load in the sample extraction exclusion list
     * \param input the file name
     * \param ignore_fid whether we should ignore the FID (use 2 column or
     * 1) \return an unordered_set use for checking if the sample is in the
     * file
     */
    std::unordered_set<std::string>
    load_ref(std::unique_ptr<std::istream> input, bool ignore_fid);
    bool
    not_in_xregion(const std::vector<IITree<size_t, size_t>>& exclusion_regions,
                   const std::unique_ptr<SNP>& base,
                   const std::unique_ptr<SNP>& target);
    bool check_rs(const std::string& snpid, const std::string& chrid,
                  std::string& rsid,
                  std::unordered_set<std::string>& processed_snps,
                  std::unordered_set<std::string>& duplicated_snps,
                  Genotype* genotype);
    bool check_ambig(const std::string& a1, const std::string& a2,
                     const std::string& ref, bool& flipping);

    bool check_chr(const std::string& chr_str, std::string& prev_chr,
                   size_t& chr_num, bool& chr_error, bool& sex_error);
    bool
    process_snp(const std::vector<IITree<size_t, size_t>>& exclusion_regions,
                const std::string& mismatch_snp_record_name,
                const std::string& mismatch_source, const std::string& snpid,
                const std::unique_ptr<SNP>& snp,
                std::unordered_set<std::string>& processed_snps,
                std::unordered_set<std::string>& duplicated_snps,
                std::vector<bool>& retain_snp, Genotype* genotype);
    void shrink_snp_vector(const std::vector<bool>& retain)
    {
        m_existed_snps.erase(
            std::remove_if(m_existed_snps.begin(), m_existed_snps.end(),
                           [&retain, this](const std::unique_ptr<SNP>& s) {
                               return !retain[(&s - &*begin(m_existed_snps))];
                           }),
            m_existed_snps.end());
        m_existed_snps.shrink_to_fit();
    }
    void shrink_snp_vector(const std::vector<std::atomic<bool>>& retain)
    {
        m_existed_snps.erase(
            std::remove_if(m_existed_snps.begin(), m_existed_snps.end(),
                           [&retain, this](const std::unique_ptr<SNP>& s) {
                               return !retain[(&s - &*begin(m_existed_snps))];
                           }),
            m_existed_snps.end());
        m_existed_snps.shrink_to_fit();
    }

    /*!
     * \brief Function to load in SNP extraction exclusion list
     * \param input the file name of the SNP list
     * \param reporter the logger
     * \returnan unordered_set use for checking if the SNP is in the file
     */
    std::unordered_set<std::string>
    load_snp_list(std::unique_ptr<std::istream> input);
    size_t get_rs_column(const std::string& input);
    /** Misc information **/
    /*!
     * \brief Function to check if the two alleles are ambiguous
     * \param ref_allele the reference allele
     * \param alt_allele the alternative allele
     * \return true if it is ambiguous
     */
    inline bool ambiguous(const std::string& ref_allele,
                          const std::string& alt_allele) const
    {
        // the allele should all be in upper case but whatever
        // true if equal
        if (ref_allele.empty() || alt_allele.empty()) return false;
        if (ref_allele == alt_allele) return true;
        return (ref_allele == "A" && alt_allele == "T")
               || (alt_allele == "A" && ref_allele == "T")
               || (ref_allele == "G" && alt_allele == "C")
               || (alt_allele == "G" && ref_allele == "C");
    }
};

#endif /* SRC_GENOTYPE_HPP_ */
