﻿// This file is part of PRSice-2, copyright (C) 2016-2019
// Shing Wan Choi, Paul F. O’Reilly
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "genotype.hpp"

std::string Genotype::print_duplicated_snps(
    const std::unordered_set<std::string>& duplicated_snp,
    const std::string& out_prefix)
{
    // there are duplicated SNPs, we will need to terminate with the
    // information
    std::ofstream log_file_stream;
    std::string dup_name = out_prefix + ".valid";
    log_file_stream.open(dup_name.c_str());
    if (!log_file_stream.is_open())
    { throw std::runtime_error("Error: Cannot open file: " + dup_name); }
    // we should not use m_existed_snps unless it is for reference
    for (auto&& snp : m_existed_snps)
    {
        // we only output the valid SNPs.
        if (duplicated_snp.find(snp->rs()) == duplicated_snp.end()
            && (!m_has_chr_id_formula
                || duplicated_snp.find(get_chr_id(snp))
                       == duplicated_snp.end()))
            log_file_stream << snp->rs() << "\t" << snp->chr() << "\t"
                            << snp->loc() << "\t" << snp->ref() << "\t"
                            << snp->alt() << "\n";
    }
    log_file_stream.close();
    return std::string(
        "Error: A total of " + std::to_string(duplicated_snp.size())
        + " duplicated SNP ID detected out of "
        + misc::to_string(m_existed_snps.size())
        + " input SNPs! Valid SNP ID (post --extract / "
          "--exclude, non-duplicated SNPs) stored at "
        + dup_name + ". You can avoid this error by using --extract "
        + dup_name);
}


// std::mutex Genotype::m_mutex;
std::vector<std::string> Genotype::set_genotype_files(const std::string& prefix)
{
    std::vector<std::string> genotype_files;
    if (prefix.find("#") != std::string::npos)
    {
        for (size_t chr = 1; chr <= m_autosome_ct; ++chr)
        {
            std::string name = prefix;
            misc::replace_substring(name, "#", std::to_string(chr));
            genotype_files.push_back(name);
        }
    }
    else
    {
        genotype_files.push_back(prefix);
    }
    return genotype_files;
}

void Genotype::add_flags(const std::vector<IITree<size_t, size_t>>& gene_sets,
                         const size_t num_sets,
                         const bool genome_wide_background)
{
    const size_t required_size = BITCT_TO_WORDCT(num_sets);
    for (auto&& snp : m_existed_snps)
    { construct_flag(gene_sets, required_size, genome_wide_background, snp); }
}

void Genotype::snp_extraction(const std::string& extract_snps,
                              const std::string& exclude_snps)
{
    if (!extract_snps.empty())
    {
        m_exclude_snp = false;
        auto input = misc::load_stream(extract_snps);
        m_snp_selection_list = load_snp_list(std::move(input));
    }
    else if (!exclude_snps.empty())
    {
        auto input = misc::load_stream(exclude_snps);
        m_snp_selection_list = load_snp_list(std::move(input));
    }
}

bool Genotype::has_parent(const std::unordered_set<std::string>& founder_info,
                          const std::vector<std::string>& token,
                          const std::string& fid, const size_t idx)
{

    if (idx == ~size_t(0)) return false;
    auto found = founder_info.find(fid + m_delim + token.at(idx));
    return found != founder_info.end();
}


bool Genotype::check_chr(const std::string& chr_str, std::string& prev_chr,
                         size_t& chr_num, bool& chr_error, bool& sex_error)
{
    if (chr_str != prev_chr)
    {
        auto chr_code = get_chrom_code(chr_str);
        if (chr_code < 0)
        {
            if (!chr_error)
                m_reporter->report("Error: Invalid chromosome number for SNP");
            chr_error = true;
            return false;
        }
        if (chr_code > static_cast<int>(m_autosome_ct)
            || is_set(m_haploid_mask.data(), static_cast<uint32_t>(chr_code)))
        {
            // this is sex / mt chromosome
            if (!sex_error)
                m_reporter->report("Warning: Currently not support "
                                   "haploid chromosome and sex "
                                   "chromosomes\n");
            sex_error = true;
            return false;
        }
        chr_num = static_cast<size_t>(chr_code);
        prev_chr = chr_str;
    }
    return true;
}
bool Genotype::check_rs(const std::string& snpid, const std::string& chrid,
                        std::string& rsid,
                        std::unordered_set<std::string>& processed_snps,
                        std::unordered_set<std::string>& duplicated_snps,
                        Genotype* genotype)
{
    if ((snpid.empty() || snpid == ".") && (rsid.empty() || rsid == "."))
    {
        ++m_base_missed;
        return false;
    }
    auto&& find_rs = genotype->m_existed_snps_index.find(rsid);
    if (find_rs == genotype->m_existed_snps_index.end())
    {
        if (snpid.empty()
            || genotype->m_existed_snps_index.find(snpid)
                   == genotype->m_existed_snps_index.end())
        {
            if (chrid.empty()
                || genotype->m_existed_snps_index.find(chrid)
                       == genotype->m_existed_snps_index.end())
            {
                ++m_base_missed;
                return false;
            }
            rsid = chrid;
        }
        else
        {
            rsid = snpid;
        }
    }
    if (processed_snps.find(rsid) != processed_snps.end())
    {
        // no need to add m_base_missed as this will completley error out
        duplicated_snps.insert(rsid);
        return false;
    }
    return true;
}

bool Genotype::check_ambig(const std::string& a1, const std::string& a2,
                           const std::string& ref, bool& flipping)
{
    bool ambig = ambiguous(a1, a2);
    if (ambig)
    {
        ++m_num_ambig;
        if (!m_keep_ambig) return false;
        // we assume allele matching has been done
        // therefore ref = a1 or complementary ref = a1
        // or alt = a1 or complementary(alt = a1)
        // but as this is ambiguous, the complemenary check will always return
        // true AT match to TA without need of flipping (as comp TA = AT)
        // but we want to flip this (we want AT to match to AT)
        // so we will check if a1 == ref and only flip when that is not true
        flipping = (a1 != ref);
    }
    return true;
}
bool Genotype::not_in_xregion(
    const std::vector<IITree<size_t, size_t>>& exclusion_regions,
    const std::unique_ptr<SNP>& base, const std::unique_ptr<SNP>& target)
{
    if (base->chr() != ~size_t(0) && base->loc() != ~size_t(0)) return true;
    if (Genotype::within_region(exclusion_regions, target->chr(),
                                target->loc()))
    {
        ++m_num_xrange;
        return false;
    }
    return true;
}
std::string
Genotype::chr_id_from_genotype(const std::unique_ptr<SNP>& snp) const
{
    std::string chr_id = "";
    if (!m_has_chr_id_formula) return chr_id;

    for (auto&& col : m_chr_id_column)
    {
        if (col < 0)
        {
            auto idx = -1 * col - 1;
            chr_id += m_chr_id_symbol[idx];
        }
        else
        {
            switch (col)
            {
            case +BASE_INDEX::CHR: chr_id += std::to_string(snp->chr()); break;
            case +BASE_INDEX::BP: chr_id += std::to_string(snp->loc()); break;
            case +BASE_INDEX::EFFECT: chr_id += snp->ref(); break;
            case +BASE_INDEX::NONEFFECT: chr_id += snp->alt(); break;
            default: throw std::logic_error("Error: This should never happen");
            }
        }
    }
    return chr_id;
}
bool Genotype::process_snp(
    const std::vector<IITree<size_t, size_t>>& exclusion_regions,
    const std::string& mismatch_snp_record_name,
    const std::string& mismatch_source, const std::string& snpid,
    const std::unique_ptr<SNP>& snp,
    std::unordered_set<std::string>& processed_snps,
    std::unordered_set<std::string>& duplicated_snps,
    std::vector<bool>& retain_snp, Genotype* genotype)
{
    misc::to_upper(snp->ref());
    misc::to_upper(snp->alt());
    auto chr_id = chr_id_from_genotype(snp);
    if (!check_rs(snpid, chr_id, snp->rs(), processed_snps, duplicated_snps,
                  genotype))
        return false;
    auto snp_idx = genotype->m_existed_snps_index[snp->rs()];
    auto&& target_snp = genotype->m_existed_snps[snp_idx];

    bool flipping = false;
    if (!target_snp->matching(snp, flipping))
    {
        genotype->print_mismatch(mismatch_snp_record_name, mismatch_source,
                                 target_snp, snp);
        ++m_num_ref_target_mismatch;
        return false;
    }
    if (!check_ambig(snp->ref(), snp->alt(), target_snp->ref(), flipping))
        return false;
    // only do region test if we know we haven't done it during read_base
    // we will do it in read_base if we have chr and loc info.
    if (!not_in_xregion(exclusion_regions, target_snp, snp)) { return false; }
    //  only add valid SNPs
    processed_snps.insert(snp->rs());
    target_snp->add_snp_info(snp, flipping, m_is_ref);
    retain_snp[snp_idx] = true;
    return true;
}

void Genotype::gen_sample(const size_t fid_idx, const size_t iid_idx,
                          const size_t sex_idx, const size_t dad_idx,
                          const size_t mum_idx, const size_t cur_idx,
                          const std::unordered_set<std::string>& founder_info,
                          const std::string& pheno,
                          std::vector<std::string>& token,
                          std::vector<Sample_ID>& sample_storage,
                          std::unordered_set<std::string>& sample_in_file,
                          std::vector<std::string>& duplicated_sample_id)
{
    assert(m_vector_initialized);
    for (size_t i = 0; i < token.size(); ++i) { misc::trim(token[i]); }
    // we have already checked for malformed file
    const std::string fid = (m_ignore_fid) ? "-" : token[fid_idx];
    const std::string id =
        (m_ignore_fid) ? token[iid_idx] : fid + m_delim + token[iid_idx];
    if (m_max_fid_length < token[fid_idx].length())
        m_max_fid_length = token[fid_idx].length();
    if (m_max_iid_length < token[iid_idx].length())
        m_max_iid_length = token[iid_idx].length();

    // end immediately if duplicated samples are found
    if (sample_in_file.find(id) != sample_in_file.end())
    {
        duplicated_sample_id.push_back(id);
        return;
    }
    auto&& find_id =
        m_sample_selection_list.find(id) != m_sample_selection_list.end();
    bool inclusion = m_remove_sample ^ find_id;
    bool in_regression = false;
    // we can't check founder if there isn't fid

    const bool has_father = has_parent(founder_info, token, fid, dad_idx);
    const bool has_mother = has_parent(founder_info, token, fid, mum_idx);
    if (inclusion)
    {
        if (!m_ignore_fid && (has_father || has_mother))
        {
            // we still calculate PRS for this sample
            SET_BIT(cur_idx, m_calculate_prs.data());
            ++m_num_non_founder;
            // but will only include it in the regression model if users asked
            // to include non-founders
            in_regression = m_keep_nonfounder;
        }
        else
        {
            ++m_founder_ct;
            SET_BIT(cur_idx, m_sample_for_ld.data());
            SET_BIT(cur_idx, m_calculate_prs.data());
            in_regression = true;
        }
    }
    m_sample_ct += inclusion;
    // TODO: Better sex parsing? Can also be 0, 1 or F and M
    if (sex_idx != ~size_t(0) && token[sex_idx] == "1") { ++m_num_male; }
    else if (sex_idx != ~size_t(0) && token[sex_idx] == "2")
    {
        ++m_num_female;
    }
    else
    {
        ++m_num_ambig_sex;
    }
    // this must be incremented within each loop
    if (inclusion && !m_is_ref)
    {
        sample_storage.emplace_back(
            Sample_ID(token[fid_idx], token[iid_idx], pheno, in_regression));
    }
    sample_in_file.insert(id);
}

std::vector<std::string>
Genotype::load_genotype_prefix(std::unique_ptr<std::istream> in)
{
    std::vector<std::string> genotype_files;
    std::string line;
    while (std::getline(*in, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        genotype_files.push_back(line);
    }
    // we no longer need in
    in.reset();
    return genotype_files;
}

void Genotype::init_chr(int num_auto, bool no_x, bool no_y, bool no_xy,
                        bool no_mt)
{
    // this initialize haploid mask as the maximum possible number
    m_xymt_codes.resize(XYMT_OFFSET_CT);
    m_haploid_mask.resize(CHROM_MASK_WORDS, 0);
    if (num_auto < 0)
    {
        // not required at the moment
        /*
        num_auto = -num_auto;
        m_autosome_ct = static_cast<uint32_t>(num_auto);
        m_xymt_codes[X_OFFSET] = -1;
        m_xymt_codes[Y_OFFSET] = -1;
        m_xymt_codes[XY_OFFSET] = -1;
        m_xymt_codes[MT_OFFSET] = -1;
        m_max_code = static_cast<uint32_t>(num_auto);
        fill_all_bits((static_cast<uint32_t>(num_auto)) + 1,
                      m_haploid_mask.data());*/
    }
    else
    {
        uint32_t unsign_num_auto = static_cast<uint32_t>(num_auto);
        m_autosome_ct = unsign_num_auto;
        m_xymt_codes[X_OFFSET] = num_auto + 1;
        m_xymt_codes[Y_OFFSET] = num_auto + 2;
        m_xymt_codes[XY_OFFSET] = num_auto + 3;
        m_xymt_codes[MT_OFFSET] = num_auto + 4;
        set_bit(unsign_num_auto + 1, m_haploid_mask.data());
        set_bit(unsign_num_auto + 2, m_haploid_mask.data());
        if (no_x)
        {
            m_xymt_codes[X_OFFSET] = -1;
            clear_bit(unsign_num_auto + 1, m_haploid_mask.data());
        }
        if (no_y)
        {
            m_xymt_codes[Y_OFFSET] = -1;
            clear_bit(unsign_num_auto + 2, m_haploid_mask.data());
        }
        if (no_xy) { m_xymt_codes[XY_OFFSET] = -1; }
        if (no_mt) { m_xymt_codes[MT_OFFSET] = -1; }
        if (m_xymt_codes[MT_OFFSET] != -1) { m_max_code = unsign_num_auto + 4; }
        else if (m_xymt_codes[XY_OFFSET] != -1)
        {
            m_max_code = unsign_num_auto + 3;
        }
        else if (m_xymt_codes[Y_OFFSET] != -1)
        {
            m_max_code = unsign_num_auto + 2;
        }
        else if (m_xymt_codes[X_OFFSET] != -1)
        {
            m_max_code = unsign_num_auto + 1;
        }
        else
        {
            m_max_code = unsign_num_auto;
        }
    }
    /*
    fill_all_bits(m_autosome_ct + 1, m_chrom_mask.data());
    for (uint32_t xymt_idx = 0; xymt_idx < XYMT_OFFSET_CT; ++xymt_idx) {
        int32_t cur_code = m_xymt_codes[xymt_idx];
        if (cur_code != -1) {
            set_bit(m_xymt_codes[xymt_idx], m_chrom_mask.data());
        }
    }
    m_chrom_start.resize(m_max_code); // 1 extra for the info
    */
}

size_t Genotype::get_rs_column(const std::string& input)
{
    std::vector<std::string> token = misc::split(input);
    // rs_index store the location of the RS ID
    if (token.size() != 1)
    {
        size_t rs_index = 0;
        for (auto&& name : token)
        {
            misc::to_upper(name);
            if (name == "SNP" || name == "RS" || name == "RS_ID"
                || name == "RS.ID" || name == "RSID" || name == "VARIANT.ID"
                || name == "VARIANT_ID" || name == "SNP_ID" || name == "SNP.ID")
            {
                m_reporter->report(name
                                   + " assume to be column containing SNP ID");
                return rs_index;
            }
            ++rs_index;
        }
        // if user accidentally add an empty column at the back of their bim
        // file
        // now it is possible that the file has an empty column for header, but
        // that will just be an edge case and can be easily solved by adding the
        // appropriate header
        if (token.size() == 6 || (token.size() == 7 && token.back().empty()))
        {
            m_reporter->report(
                "SNP extraction/exclusion list contains 6 columns, "
                "will assume this is a bim file, with the "
                "second column contains the SNP ID");
            return 1;
        }
        else
        {
            m_reporter->report(
                "SNP extraction/exclusion list contains "
                + misc::to_string(token.size())
                + " columns, "
                  "will assume first column contains the SNP ID");
            return 0;
        }
    }
    else
    {
        m_reporter->report(
            "Only one column detected, will assume only SNP ID is provided");
        return 0;
    }
}

std::unordered_set<std::string>
Genotype::load_snp_list(std::unique_ptr<std::istream> input)
{
    std::string line;
    std::getline(*input, line);
    input->clear();
    input->seekg(0, std::ios::beg);
    misc::trim(line);
    size_t rs_index = get_rs_column(line);
    std::vector<std::string_view> token;
    std::unordered_set<std::string> result;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        token = misc::tokenize(line);
        result.insert(std::string(token[rs_index]));
    }
    input.reset();
    return result;
}

void Genotype::load_samples(bool verbose)
{
    if (!m_remove_file.empty())
    {
        auto input = misc::load_stream(m_remove_file);
        m_sample_selection_list = load_ref(std::move(input), m_ignore_fid);
    }
    else if (!m_keep_file.empty())
    {
        m_remove_sample = false;
        auto input = misc::load_stream(m_keep_file);
        m_sample_selection_list = load_ref(std::move(input), m_ignore_fid);
    }
    if (!m_is_ref) { m_sample_id = gen_sample_vector(); }
    else
    {
        // don't bother loading up the sample vector as it should
        // never be used for reference panel (except for the founder_info)
        gen_sample_vector();
    }
    std::string message = misc::to_string(m_unfiltered_sample_ct) + " people ("
                          + misc::to_string(m_num_male) + " male(s), "
                          + misc::to_string(m_num_female)
                          + " female(s)) observed\n";
    message.append(misc::to_string(m_founder_ct) + " founder(s) included");

    const uintptr_t unfiltered_sample_ctl =
        BITCT_TO_WORDCT(m_unfiltered_sample_ct);
    m_exclude_from_std.resize(unfiltered_sample_ctl, 0);
    if (verbose) m_reporter->report(message);
}


void Genotype::print_mismatch(const std::string& out, const std::string& type,
                              const std::unique_ptr<SNP>& target,
                              const std::unique_ptr<SNP>& new_snp)
{
    // mismatch found between base target and reference
    if (!m_mismatch_snp_record.is_open())
    {
        m_mismatch_snp_record.open(out.c_str());
        if (!m_mismatch_snp_record.is_open())
        {
            throw std::runtime_error(std::string("Cannot open mismatch file to "
                                                 "write: "
                                                 + out));
        }
        m_mismatch_snp_record << "File_Type\tRS_ID\tCHR_Target\tCHR_"
                                 "File\tBP_Target\tBP_File\tA1_"
                                 "Target\tA1_File\tA2_Target\tA2_"
                                 "File\n";
    }
    m_mismatch_snp_record << type << "\t" << new_snp->rs() << "\t"
                          << new_snp->chr() << "\t";
    if (target->chr() == ~size_t(0)) { m_mismatch_snp_record << "-\t"; }
    else
    {
        m_mismatch_snp_record << target->chr() << "\t";
    }
    m_mismatch_snp_record << new_snp->loc() << "\t";
    if (target->loc() == ~size_t(0)) { m_mismatch_snp_record << "-\t"; }
    else
    {
        m_mismatch_snp_record << target->loc() << "\t";
    }
    m_mismatch_snp_record << new_snp->ref() << "\t" << target->ref() << "\t"
                          << new_snp->alt() << "\t" << target->alt()
                          << std::endl;
}

void Genotype::load_snps(
    const std::string& out,
    const std::vector<IITree<size_t, size_t>>& exclusion_regions, bool verbose)
{
    m_base_missed = 0;
    m_num_ambig = 0;
    m_num_xrange = 0;
    gen_snp_vector(exclusion_regions, out);
    m_marker_ct = m_existed_snps.size();
    std::string message = "";
    if (m_base_missed != 0)
    {
        message.append(std::to_string(m_base_missed)
                       + " variant(s) not found in previous data\n");
    }
    std::string action = "excluded";
    if (m_keep_ambig) action = "kept";
    if (m_num_ref_target_mismatch != 0)
    {
        message.append(std::to_string(m_num_ref_target_mismatch)
                       + " variant(s) with mismatch information\n");
    }
    if (m_num_ambig != 0)
    {
        message.append(std::to_string(m_num_ambig) + " ambiguous variant(s) "
                       + action + "\n");
    }
    if (m_num_xrange != 0)
    {
        message.append(std::to_string(m_num_xrange)
                       + " variant(s) removed as they fall within the "
                         "--x-range region(s)\n");
    }
    message.append(std::to_string(m_marker_ct) + " variant(s) included\n");

    if (verbose) m_reporter->report(message);
    m_snp_selection_list.clear();
    if (m_marker_ct == 0)
    {
        message = "Error: No vairant remained!\n";
        throw std::runtime_error(message);
    }
    // only remove selection list here, as we might need this for bgen file
    // check
    m_sample_selection_list.clear();
}


Genotype::~Genotype() {}
std::vector<std::pair<size_t, size_t>> Genotype::get_chrom_boundary()
{
    std::vector<std::pair<size_t, size_t>> chrom_bound;
    std::uint32_t prev_chr = 0;
    auto total = m_existed_snps.size();
    for (size_t i = 0; i < total; ++i)
    {
        auto&& cur_snp = m_existed_snps[m_sort_by_p_index[i]];
        if (cur_snp->chr() != prev_chr)
        {
            if (!chrom_bound.empty())
            {
                // new chr
                std::get<1>(chrom_bound.back()) = i;
            }
            chrom_bound.push_back(std::pair<size_t, size_t>(i, total));
            prev_chr = cur_snp->chr();
        }
    }

    return chrom_bound;
}
// TODO: This function is likely to have bug (2019-12-09 It does)
void Genotype::build_membership_matrix(
    const size_t num_sets, const std::vector<std::string>& region_name,
    std::unique_ptr<std::ostream> out)
{
    // set_thresholds contain the thresholds in each set.
    if (num_sets != region_name.size())
    {
        throw std::runtime_error("Error: Number of set(s) does not match the "
                                 "number of region name!");
    }
    // Structure = std::vector<std::set<double>>
    (*out) << "CHR\tBP\tSNP\tCM";
    for (size_t i = 0; i < region_name.size(); ++i)
    { (*out) << "\t" << region_name[i]; }
    (*out) << "\n";
    for (size_t i_snp = 0; i_snp < m_existed_snps.size(); ++i_snp)
    {
        auto&& snp = m_existed_snps[i_snp];
        auto&& flags = snp->get_flag();
        (*out) << snp->chr() << "\t" << snp->loc() << "\t" << snp->rs() << "\t"
               << 0;
        for (size_t s = 0; s < num_sets; ++s)
        { (*out) << "\t" << IS_SET(flags.data(), s); }
        (*out) << "\n";
    }
}

std::unordered_set<std::string>
Genotype::load_ref(std::unique_ptr<std::istream> input, bool ignore_fid)
{
    std::string line;
    // now go through the sample file. We require the FID (if any) and IID  must
    // be the first 1/2 column of the file
    std::vector<std::string> token;
    std::unordered_set<std::string> result;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        token = misc::split(line);
        if (ignore_fid) { result.insert(token[0]); }
        else
        {
            if (token.size() < 2)
                throw std::runtime_error(
                    "Error: Require FID and IID for extraction. "
                    "You can ignore the FID by using the --ignore-fid flag");
            result.insert(token[0] + m_delim + token[1]);
        }
    }
    input.reset();
    return result;
}
