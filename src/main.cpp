// This file is part of PRSice-2, copyright (C) 2016-2019
// Shing Wan Choi, Paul F. O’Reilly
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "IITree.h"
#include "binaryplink.hpp"
#include "cgranges.h"
#include "commander.hpp"
#include "genotype.hpp"
#include "plink_common.hpp"
#include "region.hpp"
#include "reporter.hpp"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>

int main(int argc, char* argv[])
{
    const std::string separator =
        "==================================================";
    // initialize reporter, use to generate log
    Reporter reporter;
    try
    {
        // initialize commander, use to parse command line arguments
        // initialization should help setting the default values
        Commander commander;
        try
        {
            if (!commander.process_command(argc, argv, reporter))
            {
                return 0; // only require the usage information
            }
        }
        catch (...)
        {
            return -1; // all error messages should have printed
        }
        // parse the exclusion range and put it into the exclusion object
        // Generate the exclusion region
        std::vector<IITree<size_t, size_t>> exclusion_regions;
        Region::generate_exclusion(exclusion_regions,
                                   commander.exclusion_range());
        Genotype* target_file =
            new BinaryPlink(commander.get_target(), commander.get_pheno(),
                            commander.delim(), &reporter);
        target_file =
            &target_file->keep_nonfounder(commander.nonfounders())
                 .keep_ambig(commander.keep_ambig())
                 .intermediate(commander.use_inter())
                 .set_prs_instruction(commander.get_prs_instruction())
                 .set_weight(commander.get_prs_instruction().genetic_model);

        target_file->snp_extraction(commander.extract_file(),
                                    commander.exclude_file());


        reporter.report("Loading Genotype info\n" + separator);

        target_file->load_samples();
        const bool verbose = true;
        target_file->load_snps(commander.out(), exclusion_regions, verbose);
        if (target_file->num_snps() == 0)
        {
            reporter.report("No SNPs left for PRSice processing");
            return -1;
        }
        Region region(commander.get_set(), &reporter);
        const size_t num_regions = region.generate_regions(
            target_file->included_snps_idx(), target_file->included_snps(),
            target_file->max_chr());
        target_file->add_flags(region.get_gene_sets(), num_regions,
                               commander.get_set().full_as_background);
        auto region_names = region.get_names();
        auto snp_file = misc::load_ostream(commander.out() + ".snp");
        target_file->build_membership_matrix(num_regions, region_names,
                                             std::move(snp_file));
        delete target_file;
    }
    catch (const std::exception& ex)
    {
        reporter.report(ex.what());
    }
    catch (...)
    {
        std::string error_message = "Error: Bad Allocation exception detected. "
                                    "This is likely due to insufficient memory "
                                    "for PRSice. You can try re-running PRSice "
                                    "with more memory.";
        reporter.report(error_message);
    }
    return 0;
}
