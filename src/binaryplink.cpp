﻿// This file is part of PRSice-2, copyright (C) 2016-2019
// Shing Wan Choi, Paul F. O’Reilly
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "binaryplink.hpp"

BinaryPlink::BinaryPlink(const GenoFile& geno, const Phenotype& pheno,
                         const std::string& delim, Reporter* reporter)
{
    const std::string message = initialize(geno, pheno, delim, "bed", reporter);
    if (m_sample_file.empty())
    { m_sample_file = m_genotype_file_names.front() + ".fam"; }
    m_reporter->report(message);
    m_hard_coded = true; // technically true
}

std::unordered_set<std::string>
BinaryPlink::get_founder_info(std::unique_ptr<std::istream>& famfile)
{
    std::string line;
    std::vector<std::string> token;
    std::unordered_set<std::string> founder_info;
    while (std::getline(*famfile, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        misc::split(token, line);
        if (token.size() != 6)
        {
            throw std::runtime_error(
                "Error: Malformed fam file. Fam file should have 6 columns."
                "Line: "
                + std::to_string(m_unfiltered_sample_ct + 1) + "\n");
        }
        founder_info.insert(token[+FAM::FID] + m_delim + token[+FAM::IID]);
        ++m_unfiltered_sample_ct;
    }
    (*famfile).clear();
    (*famfile).seekg(0);
    return founder_info;
}
std::vector<Sample_ID> BinaryPlink::gen_sample_vector()
{
    assert(m_genotype_file_names.size() > 0);
    auto famfile = misc::load_stream(m_sample_file);
    m_unfiltered_sample_ct = 0;
    // will also count number of samples here. Which initialize the important
    // m_unfiltered_sample_ct
    std::unordered_set<std::string> founder_info = get_founder_info(famfile);
    init_sample_vectors();
    // we will return the sample_name
    std::vector<Sample_ID> sample_name;
    std::unordered_set<std::string> samples_in_fam;
    std::vector<std::string> duplicated_sample_id;
    // for purpose of output
    uintptr_t sample_index = 0; // this is just for error message
    std::vector<std::string> token;
    std::string line;
    while (std::getline(*famfile, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        misc::split(token, line);
        // we have already checked for malformed file
        gen_sample(+FAM::FID, +FAM::IID, +FAM::SEX, +FAM::FATHER, +FAM::MOTHER,
                   sample_index, founder_info, token[+FAM::PHENOTYPE], token,
                   sample_name, samples_in_fam, duplicated_sample_id);
        ++sample_index;
    }

    if (duplicated_sample_id.size() > 0)
    {
        throw std::runtime_error(
            "Error: A total of " + misc::to_string(duplicated_sample_id.size())
            + " duplicated samples detected!\n"
            + "Please ensure all samples have an unique identifier");
    }
    famfile.reset();
    post_sample_read_init();
    return sample_name;
}


void BinaryPlink::gen_snp_vector(
    const std::vector<IITree<size_t, size_t>>& exclusion_regions,
    const std::string& out_prefix)
{
    std::unordered_set<std::string> processed_snps;
    std::unordered_set<std::string> duplicated_snp;
    std::vector<std::string> bim_token;
    std::ifstream bim;
    std::string bim_name, chr, line, bed_name;
    std::string prev_chr = "", error_message = "";
    std::string prefix;
    size_t chr_num = 0;
    size_t num_snp_read = 0;
    bool chr_error, sex_error;
    bool valid = true;
    for (size_t idx = 0; idx < m_genotype_file_names.size(); ++idx)
    {
        // go through each bim file
        prefix = m_genotype_file_names[idx];

        bed_name = prefix + ".bed";
        // make sure we reset the flag of the ifstream by closing it before use
        auto bim = misc::load_stream(prefix + ".bim");
        num_snp_read = 0;
        prev_chr = "";
        while (std::getline(*bim, line))
        {
            valid = true;
            misc::trim(line);
            if (line.empty()) continue;
            // we need to remember the actual number read is num_snp_read+1
            ++num_snp_read;
            bim_token = misc::split(line);
            if (bim_token.size() < 6)
            {
                throw std::runtime_error(
                    "Error: Malformed bim file. Less than 6 column on "
                    "line: "
                    + misc::to_string(num_snp_read) + "\n");
            }
            // read in the chromosome string
            chr = bim_token[+BIM::CHR];
            if (!check_chr(bim_token[+BIM::CHR], prev_chr, chr_num, chr_error,
                           sex_error))
            { continue; }
            // now read in the coordinate
            size_t loc = ~size_t(0);
            try
            {
                loc = misc::string_to_size_t(bim_token[+BIM::BP].c_str());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "Error: Invalid SNP coordinate: " + bim_token[+BIM::RS]
                    + ":" + bim_token[+BIM::BP]
                    + "\nPlease check you have the correct input");
            }
            if (Genotype::within_region(exclusion_regions, chr_num, loc))
            { valid = false; }
            auto&& selection = m_snp_selection_list.find(bim_token[+BIM::RS]);
            if ((!m_exclude_snp && selection == m_snp_selection_list.end())
                || (m_exclude_snp && selection != m_snp_selection_list.end()))
            { valid = false; }
            // check if this is a duplicated SNP
            if (processed_snps.find(bim_token[+BIM::RS])
                != processed_snps.end())
            {
                duplicated_snp.insert(bim_token[+BIM::RS]);
                continue;
            }
            else
            {
                m_existed_snps.emplace_back(std::make_unique<SNP>(
                    bim_token[+BIM::RS], chr_num, loc, bim_token[+BIM::A1],
                    bim_token[+BIM::A2], idx, 0));
                processed_snps.insert(bim_token[+BIM::RS]);
            }
        }
    }
    if (duplicated_snp.size() != 0)
    {
        throw std::runtime_error(
            print_duplicated_snps(duplicated_snp, out_prefix));
    }
}

void BinaryPlink::check_bed(const std::string& bed_name, size_t num_marker,
                            uintptr_t& bed_offset)
{
    bed_offset = 3;
    uint32_t uii = 0;
    int64_t llxx = 0;
    int64_t llyy = 0;
    int64_t llzz = 0;
    uintptr_t unfiltered_sample_ct4 = (m_unfiltered_sample_ct + 3) / 4;
    std::ifstream bed(bed_name.c_str(), std::ios::binary);
    if (!bed.is_open())
    { throw std::runtime_error("Cannot read bed file: " + bed_name); }
    bed.seekg(0, bed.end);
    llxx = bed.tellg();
    if (!llxx)
    { throw std::runtime_error("Error: Empty .bed file: " + bed_name); }
    bed.clear();
    bed.seekg(0, bed.beg);
    char version_check[3];
    bed.read(version_check, 3);
    uii = static_cast<uint32_t>(bed.gcount());
    llyy = static_cast<int64_t>(unfiltered_sample_ct4 * num_marker);
    llzz =
        static_cast<int64_t>(m_unfiltered_sample_ct * ((num_marker + 3) / 4));
    bool sample_major = false;
    // compare only the first 3 bytes
    if ((uii == 3) && (!memcmp(version_check, "l\x1b\x01", 3))) { llyy += 3; }
    else if ((uii == 3) && (!memcmp(version_check, "l\x1b", 3)))
    {
        // v1.00 sample-major
        sample_major = true;
        llyy = llzz + 3;
        bed_offset = 2;
    }
    else if (uii && (*version_check == '\x01'))
    {
        // v0.99 SNP-major
        llyy += 1;
        bed_offset = 1;
    }
    else if (uii && (!(*version_check)))
    {
        // v0.99 sample-major
        sample_major = true;
        llyy = llzz + 1;
        bed_offset = 2;
    }
    else
    {
        // pre-v0.99, sample-major, no header bytes
        sample_major = true;
        if (llxx != llzz)
        {
            // probably not PLINK-format at all, so give this error instead
            // of "invalid file size"
            throw std::runtime_error(
                "Error: Invalid header bytes in .bed file: " + bed_name);
        }
        llyy = llzz;
        bed_offset = 2;
    }
    if (llxx != llyy)
    {
        if ((*version_check == '#')
            || ((uii == 3) && (!memcmp(version_check, "chr", 3))))
        {
            throw std::runtime_error("Error: Invalid header bytes in PLINK "
                                     "1 .bed file: "
                                     + bed_name
                                     + "  (Is this a UCSC "
                                       "Genome\nBrowser BED file instead?)");
        }
        else
        {
            throw std::runtime_error("Error: Invalid .bed file size for "
                                     + bed_name);
        }
    }
    if (sample_major)
    {
        throw std::runtime_error(
            "Error: Currently do not support sample major format");
    }
    bed.close();
}

BinaryPlink::~BinaryPlink() {}
